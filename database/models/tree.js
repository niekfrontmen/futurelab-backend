'use strict';
module.exports = (sequelize, DataTypes) => {
  const Tree = sequelize.define('Tree', {
    lat: DataTypes.FLOAT(18, 15),
    lon: DataTypes.FLOAT(18, 15),
    data: DataTypes.TEXT
  }, {});
  Tree.associate = function(models) {
    // associations can be defined here
  };
  return Tree;
};

4.983044506771263