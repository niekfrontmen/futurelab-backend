'use strict'
/**
 * Require our modules
 */
const axios = require('axios');
const models = require('./database/models');

const Tree = models.Tree;

const URL = 'https://ckan.dataplatform.nl/dataset/96b46ab5-7638-46bb-b416-c480170b9a84/resource/7da8e62c-78c5-40db-84c9-ea2926b4b925/download/bomen.json'

Tree.destroy({
    where: {}
});

axios.get(URL).then(async (response) => {
    return response.data.features;
})
.then((data) => {
    console.log(`${data.length} trees will be added to db...`);
    let queue = Promise.all([]);

    data.forEach((row) => {
        queue = queue.then(() => Tree.create({
            lat: row.properties.latitude,
            lon: row.properties.longitude,
            data: JSON.stringify(row.properties)
        }));
    });

    queue.then(() => console.log('All trees added successfully!'));
})
.catch(function (err) {
    throw err;
})
