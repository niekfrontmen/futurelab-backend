module.exports = {
  apps : [{
    name: "backend",
    script: "./index.js",
    env: {
      NODE_ENV: "dev",
    },
    env_production: {
      NODE_ENV: "prd",
    }
  }]
}