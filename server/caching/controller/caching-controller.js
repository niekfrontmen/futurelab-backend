'use strict';
/**
 * Require our modules
 */
const config = require('../../../config/config');
const cacheManager = require('cache-manager');
const memoryCache = cacheManager.caching({
    store: config.caching.store,
    max: config.caching.max,
    ttl: config.caching.ttl
});

module.exports = {
    // Reset the Cache
	clearCache: () => {
		return memoryCache.reset();
    },
    // Get the Cache
	getMemoryCache: () => {
		return memoryCache;
    },
    // Set in Cache
	set: async (key, value) => {
        console.log(`Setting ` + key)
        console.log(value.features.length)
		return await memoryCache.set(key, value)
    },
    // Get from Cache
	get: async (key) => {

        const test = await memoryCache.get(key)
        console.log(`test: `, test)
        console.log(`Get ` + key)
        if(test) {
            console.log(test.features.length)
        }
		return await memoryCache.get(key)
	}
};