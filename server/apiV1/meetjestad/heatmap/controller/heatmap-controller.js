'use strict'
/**
 * Require our modules
 */
const axios = require('axios');
const config = require('../../../../../config/config')

 module.exports = {
    getSensorData: async (query) => {
      const getData = () => {
        return axios.get(`${config.dataEndpoints.meetJeStad.host}/${config.dataEndpoints.meetJeStad.endpoints.sensors}`).then(function (response) {
          // handle success
          return response.data;
        })
        .catch(function (err) {
          throw err;
        })
      }

      const data = await getData()

      return data
    },
    filterByDataSet(data, filter){
      try {
        if(config.dataEndpoints.meetJeStad.dataSets[filter.sensor_set]) {
          const filteredData = []
          data.forEach(feature => {
            if(config.dataEndpoints.meetJeStad.dataSets[filter.sensor_set].includes(feature.properties.id)) {
              feature.properties.temperature = parseFloat(feature.properties.temperature);
              feature.properties.humidity = parseFloat(feature.properties.humidity);
              filteredData.push(feature)
            }
          });
          return filteredData
        } else {
          return data
        }
      } catch (err) {

      }



    }
 }