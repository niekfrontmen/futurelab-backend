'use strict';
/**
 * Require our Modules
 */
const config = require('../../../../config/config')
const heatmap = require('express').Router();
const axios = require('axios');
const logger = require('../../../utils/logger');
const passport = require('passport');
const passportController = require('../../../middleware/passport/controller/passport-controller');
const controller = require('./controller/heatmap-controller');

heatmap.get('/', async (req, res) => {
  try {
    const data = await controller.getSensorData()
    const filteredData = await controller.filterByDataSet(data.features, req.query)
    res.json(filteredData)
  } catch (err) {
    res.status(500).send(err)
  }
});

// Export the heatmap
module.exports = heatmap;