'use strict';
/**
 * Require our Modules
 */
const axios = require('axios')
const sensorsRecent = require('express').Router();
const logger = require('../../../utils/logger');
const passport = require('passport');
const passportController = require('../../../middleware/passport/controller/passport-controller');
const controller = require('./controller/heatmap-controller');

sensorsRecent.get('/', (req, res) => {

  axios.get('https://meetjestad.net/data/sensors.php').then(function (response) {
    // handle success
    console.log(response);
  })
  .catch(function (error) {
    // handle error
    console.log(error);
  })
  .finally(function () {
    // always executed
  });
  // https://meetjestad.net/data/sensors_recent.php?sensors=487,249&limit=50
  // http://meetjestad.net/?layer=hittekaart&ids=227,367,210,372,241,366,374,360,395,357,350,362,376,245,217&start=&end=

});

// Export the heatmap
module.exports = sensorsRecent;