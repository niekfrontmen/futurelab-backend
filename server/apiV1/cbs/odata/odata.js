'use strict';
/**
 * Require our Modules
 */
const config = require('../../../../config/config')
const odata = require('express').Router();
const axios = require('axios');
const logger = require('../../../utils/logger');
const oData = require('odata-client');

odata.get('/', async (req, res) => {
  try {
    const q = await oData({service: 'https://opendata.cbs.nl/ODataApi/OData', version: '3.0', resources: '83981NED', format: 'json'});

    console.log(`Q: `)
    console.log(q)
    q.get().then(function(response) {
      res.send(response.body)
    });

  } catch (err) {
    console.log(err)
    res.status(500).send(err)
  }
});

// Export the odata
module.exports = odata;