"use strict";
/**
 * Require our Modules
 */
const oData = require("odata-client");

const applyFilters = (q, queryObj = {}) => {
  for (let key of Object.keys(queryObj)) {
    const values = Array.isArray(queryObj[key])
      ? queryObj[key]
      : [queryObj[key]];
    q = values.reduce(
      (q, value) => q.filter(`substringof('${value}', Perioden)`),
      q
    );
  }
  return q;
};

const odataFactory = cbsIdentifier => {
  const router = require("express").Router();

  /**
   * Get data from a dataset
   */
  router.get("/", async (req, res) => {
    try {
      let q = oData({
        service: `https://opendata.cbs.nl/ODataApi/OData/${cbsIdentifier}`,
        version: "3.0",
        resources: "TypedDataSet",
        format: "json"
      }).filter("RegioS", "=", "GM0855"); // Filter by Region = Tilburg

      // Apply other filters given in URL like /odata/tilburg/dataset?FilterProp=SomeValue
      q = applyFilters(q, req.query);

      console.log("Q", q.query());
      const cbsResponse = await q.get();
      const data = JSON.parse(cbsResponse.body).value;
      res.send(data);
    } catch (err) {
      console.log(err);
      res.status(500).send(err);
    }
  });

  /**
   * Get a description of a dataset and the properties you can filter on
   */
  router.get("/describe", async (req, res) => {
    try {
      const q = oData({
        service: `https://opendata.cbs.nl/ODataApi/OData/${cbsIdentifier}`,
        version: "3.0",
        resources: "DataProperties",
        format: "json"
      }).filter("substringof('Dimension', Type)");

      console.log("Q", q.query());
      const cbsResponse = await q.get();
      const data = JSON.parse(cbsResponse.body).value;
      await data.reduce(
        (prev, dimension) =>
          prev
            .then(() => {
              const q = oData({
                service: `https://opendata.cbs.nl/ODataApi/OData/${cbsIdentifier}`,
                version: "3.0",
                resources: dimension.Key,
                format: "json"
              });
              console.log("Dimension", dimension.Key, q.query());
              return q.get();
            })
            .then(cbsResponse => {
              dimension.values = JSON.parse(cbsResponse.body).value.map(
                ({ Key, Title }) => ({ Key, Title })
              );
            }),
        Promise.resolve()
      );
      res.send(data);
    } catch (err) {
      console.log(err);
      res.status(500).send(err);
    }
  });

  return router;
};

// Datasets containing data for region Tilburg
const routes = [
  {
    route: "/bevolking",
    identifier: "70072ned",
  },
  {
    route: "/misdaad",
    identifier: "83648NED",
  },
  {
    route: "/veiligheidsgevoel",
    identifier: "81877NED",
  },
  {
    route: "/bodemgebruik",
    identifier: "70262ned",
  },
  {
    route: "/voorzieningen",
    identifier: "80305ned",
  },
  {
    route: "/bijstandsvorderingen",
    identifier: "82020NED",
  },
  {
    route: "/verhuizingen",
    identifier: "60048ned",
  },
  {
    route: "/overledenen",
    identifier: "80142ned",
  },
  {
    route: "/bevolking-kort",
    identifier: "70648ned",
  },
  {
    route: "/speciaal-onderwijs",
    identifier: "71478ned",
  },
  {
    route: "/diefstal",
    identifier: "83651NED",
  },
  {
    route: "/zonnestroom",
    identifier: "84518NED",
  },
  {
    route: "/gezondheid-2016",
    identifier: "83674NED",
  },
  {
    route: "/geboortes",
    identifier: "82056NED",
  },
  {
    route: "/overledenen-detail",
    identifier: "03747",
  },
  {
    route: "/huishoudens",
    identifier: "71486ned",
  },
  {
    route: "/bevolking-nationaliteit",
    identifier: "70634ned",
  },
  {
    route: "/arbeidsmarkt-jongeren",
    identifier: "84455NED",
  },
  {
    route: '/motorvoertuigen',
    identifier: '37209hvv'
  },
  {
    route: '/geboorte-vruchtbaarheid',
    identifier: '37201'
  },
  {
    route: '/migratie',
    identifier: '60056ned'
  },
];

const router = require("express").Router();
routes.forEach(({ route, identifier }) =>
  router.use(route, odataFactory(identifier))
);

/** URL to describe all endpoints */
router.get("/describe", (req, res) =>
  res.send(
    routes.map(({ route, identifier }) => ({
      route: `http://localhost:3000/api/v1/odata/tilburg${route}`,
      identifier,
      describe: `http://localhost:3000/api/v1/odata/tilburg${route}/describe`,
      origin: `https://opendata.cbs.nl/ODataApi/odata/${identifier}`
    }))
  )
);

// Export the odata
module.exports = router;
