'use strict';
/**
 * Require our modules
 */
const router = require('express').Router();

router.use('/meetjestad/heatmap', require('./meetjestad/heatmap/heatmap'));
router.use('/citydata/tilburg/citydistricts', require('./ckan/tilburg/cityDistricts/cityDistricts'));
router.use('/citydata/tilburg/neighbourhoods', require('./ckan/tilburg/neighbourhoods/neighbourhoods'));
router.use('/citydata/tilburg/neighbourhood/districts', require('./ckan/tilburg/neighbourhoodsDistricts/neighbourhoodDistricts'));
router.use('/citydata/tilburg/trees', require('./ckan/tilburg/trees/trees'));

router.use('/numbeo', require('./numbeo/numbeo'));

router.use('/odata/test', require('./cbs/odata/odata'));
router.use('/odata/tilburg', require('./cbs/tilburg'));

router.use('/users/myprofile', require('./users/myprofile/myprofile'));

// Export the module
module.exports = router;