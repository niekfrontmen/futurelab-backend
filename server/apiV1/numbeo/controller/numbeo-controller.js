'use strict'
/**
 * Require our modules
 */
const axios = require('axios');
const config = require('../../../../config/config');
const cheerio = require('cheerio');
const htmlparser2 = require('htmlparser2');

const URL = 'https://www.numbeo.com/traffic/in/'

 module.exports = {
    getData: async (city) => {
      city = city[0].toUpperCase() + city.substring(1)
      const getData = (city) => {
        return axios.get(`${URL}${city}`).then(async function (response) {

          const html = response.data
          const $ = cheerio.load(html);

          // City Index
          const table = await $('.table_indices > tbody > tr');
          const cityIndex = {
            name: city,
            info: 'https://www.numbeo.com/traffic/indices_explained.jsp'
          }
          table.each(function() {
            const key = $(this).find('td:nth-child(1)').text().slice(0, -2).replace(/ /g, '_').toLowerCase().replace('_(in_minutes)', '').replace('._index', '');
            const value = parseInt($(this).find('td:nth-child(2)').text().replace('\n', ''), 10);
            if( key && value ){
              cityIndex[key] = value
            }
          });



          return {
            cityIndex
          }
        })
        .catch(function (err) {
          throw err;
        })
      }

      const data = await getData(city)

      return data
    }
 }