'use strict';
/**
 * Require our Modules
 */
const config = require('../../../config/config')
const numbeo = require('express').Router();
const logger = require('../../utils/logger');
const controller = require('./controller/numbeo-controller');

numbeo.get('/', async (req, res) => {
  const city = req.query.city || 'Tilburg'
  try {
    const data = await controller.getData(city)
    res.json(data)
  } catch (err) {
    console.log(err)
    res.status(500).send(err)
  }
});

// Export the numbeo
module.exports = numbeo;