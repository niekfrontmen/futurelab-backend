'use strict';
/**
 * Require our Modules
 */
const config = require('../../../../../config/config')
const trees = require('express').Router();
const Sequelize = require('sequelize');
const axios = require('axios');
const logger = require('../../../../utils/logger');
const controller = require('./controller/trees-controller');
const Op = Sequelize.Op;

trees.get('/', async (req, res) => {
  const page = parseInt(req.query.page || 1)
  const limit = parseInt(req.query.limit || 1000)
  const start = (page - 1) * limit
  const extendedFormat = req.query.format === 'extended'
  const { northeast_lat, northeast_lon, southwest_lat, southwest_lon } = req.query;
  let query = {};

  if (northeast_lat && northeast_lon && southwest_lat && southwest_lon) {
    query = {lat: {}, lon: {}};
    query.lat[Op.between] = [northeast_lat, southwest_lat];
    query.lon[Op.between] = [northeast_lon, southwest_lon];
  }

  try {
    let result = await controller.getData(start, limit, query, extendedFormat);
    res.json({
      paging: {
        page,
        limit,
        start,
        end: start + result.rows.length,
        count: result.rows.length,
        total: result.count
      },
      results: result.rows
    });
  } catch (err) {
    console.log(err)
    res.status(500).send(err)
  }
});

// Export the trees
module.exports = trees;