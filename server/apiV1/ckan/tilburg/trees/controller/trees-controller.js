'use strict'
/**
 * Require our modules
 */
const axios = require('axios');
const models = require('../../../../../../database/models');
const Tree = models.Tree;

module.exports = {
  getData: async (start, limit, query, extended) => {
    let attributes = ['lat', 'lon'];
    if (extended) {
      attributes.push('data');
    }
    
    return await Tree.findAndCountAll({
      attributes: attributes,
      where: query || {},
      limit: limit || 1000,
      offset: start || 0
    }).then(result => {
      if (extended) {
        result.rows = result.rows.map((row) => {
          row.data = JSON.parse(row.data);
          return row;
        });
      }
      return result;
    });
  }
}