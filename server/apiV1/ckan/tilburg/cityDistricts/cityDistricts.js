'use strict';
/**
 * Require our Modules
 */
const config = require('../../../../../config/config')
const districts = require('express').Router();
const axios = require('axios');
const logger = require('../../../../utils/logger');
const controller = require('./controller/cityDistricts-controller');

districts.get('/', async (req, res) => {
  try {
    const data = await controller.getData()
    res.json(data)
  } catch (err) {
    res.status(500).send(err)
  }
});

// Export the districts
module.exports = districts;