'use strict'
/**
 * Require our modules
 */
const axios = require('axios');
const config = require('../../../../../../config/config')

const URL = 'https://ckan.dataplatform.nl/dataset/6d599f44-5f42-4136-99e1-a59ca3582d3d/resource/7bdae670-9862-4519-a69b-dd8353f23050/download/stadsdelen.json'

 module.exports = {
   getData: async () => {
    const getData = () => {
      return axios.get(URL).then(function (response) {
        // handle success
        return response.data;
      })
      .catch(function (err) {
        throw err;
      })
    }
    const data = await getData()
    return data
   }
 }