'use strict'
/**
 * Require our modules
 */
const axios = require('axios');
const config = require('../../../../../../config/config')

const URL = 'https://ckan.dataplatform.nl/dataset/351141a1-6d3a-453b-9220-695cc499fac4/resource/6a29fad1-9be8-4e53-804b-a1fdb1dbd58d/download/buurten.json'

 module.exports = {
   getData: async () => {
    const getData = () => {
      return axios.get(URL).then(function (response) {
        // handle success
        return response.data;
      })
      .catch(function (err) {
        throw err;
      })
    }
    const data = await getData()
    return data
   }
 }