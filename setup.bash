#!/bin/bash
sequelize db:create futurelab --env=autodetect
sequelize db:migrate --env=autodetect
sequelize db:seed:all --env=autodetect
node ./preload-data.js